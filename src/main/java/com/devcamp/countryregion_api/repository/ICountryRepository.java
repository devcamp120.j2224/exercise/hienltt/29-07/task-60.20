package com.devcamp.countryregion_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregion_api.model.CCountry;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
}
