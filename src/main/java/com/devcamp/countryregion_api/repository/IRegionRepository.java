package com.devcamp.countryregion_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregion_api.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}
